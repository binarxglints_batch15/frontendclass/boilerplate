const person = {
  nama: "Rudi",
  age: 22,
  sedangKerja: false
}

// console.log(person.nama)

// const nama = person.nama
// const age = person.age
// const sedangKerja = person.sedangKerja

//destructuring
const {nama, age, sedangKerja} = person
// const nama = 'Edi'

console.log(nama)