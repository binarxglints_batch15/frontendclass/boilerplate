// 2. Variable
// Penggunaan Var
/**
 * var statusKerja = true //boolean
 * var hewanPeliharaan = ["kocheng", "hamster"] //object array
 * var nama = "Rudi" //string
 * var gender = "male" //string
 * var umur = 15 //number
 */

let nama = "Rudi" //string
let umur = 15 //number
let gender = "male" //string
let statusKerja = true //boolean
let hewanPeliharaan = ["kocheng", "hamster"] //variable object array

let hello = "hello world - dari variable"
console.log(hello)
console.log(hewanPeliharaan)

//hoisting
/**
 * dapat menyebabkan perintahnya tidak terprediksi
 */
console.log(age)
var age;


//Penulisan Variable
/**
 * 1. Dari JS nya sendiri
 *    - harus mengandung huruf, angka dan symbol $, _
 *    - karakter pertama tidak boleh angka
 *    - ada kata2 terlarang (var, class)
 * 2. Naming Conventions(dari programmer javascript di seluruh dunia)
 *    - camelCase
 */
//kata2 terlarang
/**
 * let var = "haloo";
 * let class = "halooo";
 */

//harus mengandung huruf, angka dan symbol $, _
/**
 * let first-name = "Rudi"; //error
 * let last_name = "Tabuti"; //no error
 * let name2 = "erik" //no error
 */

//Karakter pertama tidak boleh angka
/**
 * let 1name = "Bekti" //error
 * let $1name = "Bekti1"; //no error
 */

//Naming conventions - camelCase
let word1Word2
let firstName = "Rudi"
// let firstName = "rudi"

const name2 = "Rudi";
console.log(name2) //Rudi
name2 = "Rudi 2"
console.log(name2) //Rudi 2






