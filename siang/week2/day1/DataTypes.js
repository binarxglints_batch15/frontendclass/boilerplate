let nama = "Rudi" //string
let umur = 15 //number
let gender = "male" //string
let statusKerja = true //boolean
let hewanPeliharaan = ["kocheng", "hamster"] //variable object array
let data1;

console.log(typeof(data1));

// 1. String 
let string1 = "string dengan double quote";
let string2 = 'string denngan single quote';
let string3 = `string dengan bactick, ${string1}`; //template literal

console.log(string3)

// 2. Number
let number1 = 1

// 3. Boolean
let hujan = true
let banjir = false

// 4. undefined
let age;

// 5. null
let mataPelajaran = [""]; //typeof object dgn struktur data array

// 6. Object
let data2 = {} //typeof object