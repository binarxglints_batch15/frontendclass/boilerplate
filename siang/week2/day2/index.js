// Step by step challenge
/**
 * 1. Tuliskan variable untuk menyimpan kondisi perubahan background (menggunakan type data boolean, dgn value tipe data awal adalah false)
 * 2. Tuliskan masing-masing fungsi yang sudah dituliskan di dalam exercise.html
 * 3. 1 orang 1-2 fungsi (tergantung jumlah tim)
 * 4. Statement di dalam fungsi berupa DOM script utk mengganti background halaman website (ex. document.body.style.background)
 * 5. Ubah warna background sesuai dgn warna ketika button di hover (bisa di lihat di file css)
 */