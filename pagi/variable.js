/* 
Perbedaan var, let & const
1. Si var dia di Hoisting
2. Si var & si let dia bisa di re assign (di kasi nilai lagi)
3. Scope si var lebih luar daripada si let

Note : Hoisting, proses pengangkatan variable ke global 
*/



// var, let, const
var nama1 = "Mas Tintin";
let nama2 = "Mas Rafie";
const nama3 = "Mas Zaidan";

